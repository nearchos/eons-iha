self.addEventListener('install', function(event) {
    console.log('Service Worker installing.');
    event.waitUntil(
        caches.open('eons-static').then(function(cache) {
            return cache.addAll([
                'img/cloud_download.png',
                'img/eons.png',
                'img/eons_ico.png',
                'img/inspire.png',
                'img/refresh.png',
                'img/wifi.png',
                'img/wifi_off.png',
                'css/style.css',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css',
                'https://code.jquery.com/jquery-3.3.1.slim.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js',
                'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js'
                // etc
            ]);
        })
    );
});

self.addEventListener('activate', function(event) {
    console.log('Service Worker activating.');
});

self.addEventListener('fetch', function(event) {
    console.log('Service Worker activating.');
    event.respondWith(
        caches.open('eons-dynamic').then(function(cache) {
            console.log('-- accessed cache: ' + cache);
            return fetch(event.request).then(function(response) {
                cache.put(event.request, response.clone());
                return response;
            });
        })
    );
});
