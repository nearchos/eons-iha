const CURRENT_QUESTION_KEY = 'currentQuestion';

function updateIndicator() {
    // Show a different icon based on offline/online
    let online = navigator.onLine;
    // console.log('updateIndicator() - online: ' + navigator.onLine);
    document.getElementById('online').hidden = !online;
    document.getElementById('offline').hidden = online;
}

// Update the online status icon based on connectivity
window.addEventListener('online',  updateIndicator);
window.addEventListener('offline', updateIndicator);
updateIndicator();

const cacheAvailable = 'caches' in self;
if(cacheAvailable) {
    console.log("cache is available so we are good to go :-)");
} else {
    console.error("cache unavailable :-( shutting down...");
}

let questionsUrl = 'https://nearchos.gitlab.io/eons-iha/data/questions.json';

let _questionsJson = undefined;
let _currentQuestion = undefined;

// call it on load
setupApp();

function setupApp() {
    // check if a sync is needed
    if(_questionsJson === undefined) {
        sync();
    } else {
        updateVersion();
        showCurrentQuestionAndAnswers();
    }
}

function updateVersion() {
    let version = _questionsJson === undefined ? 'undefined' : _questionsJson.version;
    document.getElementById('version').innerText = version;
}

/**
 * Syncs data (questions) with web server
 */
function sync() {
    let networkDataReceived = false;
    startSpinner();

    // fetch fresh data
    let networkUpdate = fetch(questionsUrl).then(function(response) {
        return response.json();
    }).then(function(myJson) {
        networkDataReceived = true;
        populateQuestions(myJson);
    });

    // fetch cached data
    caches.match(questionsUrl).then(function(response) {
        if (!response) throw Error("No data");
        return response.json();
    }).then(function(myJson) {
        // don't overwrite newer network data
        if (!networkDataReceived) {
            populateQuestions(myJson);
        }
    }).catch(function() {
        // we didn't get cached data, the network is our last hope:
        return networkUpdate;
    }).catch(showErrorMessage).then(stopSpinner);
}

function populateQuestions(questionsJson) {
    _questionsJson = questionsJson;
    updateVersion();
    _currentQuestion = getCookie(CURRENT_QUESTION_KEY);
    if(_currentQuestion === undefined) {
        _currentQuestion = questionsJson.mainId;
        setCookie(CURRENT_QUESTION_KEY, _currentQuestion);
    }
    showCurrentQuestionAndAnswers();
}

let _currentAnswerIds = [];

function cleanUpQuestionsAndAnswers() {
    let questionsAndAnswersAndActionsContainerDiv = document.getElementById('questionsAndAnswersAndActionsContainer');
    questionsAndAnswersAndActionsContainerDiv.innerHTML = '';
}

function showCurrentQuestionAndAnswers() {
    if(_currentQuestion === undefined) {
        _currentQuestion = getCookie(CURRENT_QUESTION_KEY);
    }
    if(_currentQuestion === undefined) {
        _currentQuestion = _questionsJson.mainId;
        setCookie(CURRENT_QUESTION_KEY, _currentQuestion);
    }
    let q = getQuestion(_currentQuestion);

    // console.log('q: ' + q + ' >> ' + JSON.stringify(q));
    let questionsAndAnswersAndActionsContainerDiv = document.getElementById('questionsAndAnswersAndActionsContainer');
    if(q !== undefined) {
        let a_html = '';
        _currentAnswerIds = []; // clear
        for(let i in q.answers) {
            let a = q.answers[i];
            _currentAnswerIds.push(a.answer_id);
            a_html += answerTemplateHtml
                .replace('%submit_answer%', 'submitAnswer(' + a.answer_id + ',' + a.followUp + ',' + a.terminal + ')')
                .replace('%answer%', a.text)
                .replace('%answer-id-div%', a.answer_id)
                .replace('%answer-id-button%', a.answer_id);
        }
        let html = questionTemplateHtml.replace('%question%', q.text).replace('%answers%', a_html);
        questionsAndAnswersAndActionsContainerDiv.innerHTML = html + questionsAndAnswersAndActionsContainerDiv.innerHTML;
    }
}

function showSelectedAction(action) {
    if(_currentQuestion === undefined) {
        _currentQuestion = getCookie(CURRENT_QUESTION_KEY);
    }
    if(_currentQuestion === undefined) {
        _currentQuestion = _questionsJson.mainId;
        setCookie(CURRENT_QUESTION_KEY, _currentQuestion);
    }
    let q = getQuestion(_currentQuestion);

    let questionsAndAnswersAndActionsContainerDiv = document.getElementById('questionsAndAnswersAndActionsContainer');
    let html = actionTemplateHtml.replace('%action%', action.text);
    questionsAndAnswersAndActionsContainerDiv.innerHTML = html + questionsAndAnswersAndActionsContainerDiv.innerHTML;
}

function getQuestion(id) {
    id = parseInt(id);
    // console.log('-looking for >' + id + '< with type ' + typeof id);
    for(let i in _questionsJson.questions) {
        let q = _questionsJson.questions[i];
        // console.log('-- >' + q.id + '< with type ' + typeof q.id);
        if(q.id === id) return q;
    }
    return undefined;
}

function getAction(id) {
    id = parseInt(id);
    // console.log('-looking for >' + id + '< with type ' + typeof id);
    for(let i in _questionsJson.actions) {
        let a = _questionsJson.actions[i];
        // console.log('-- >' + a.id + '< with type ' + typeof a.id);
        if(a.id === id) return a;
    }
    return undefined;
}

function submitAnswer(answerId, followUp, terminal) {
    // console.log('-followUp >' + followUp + '< with type ' + typeof followUp);
    // console.log('-terminal >' + terminal + '< with type ' + typeof terminal);
    // todo hide unused buttons
    if(_currentAnswerIds !== undefined) {
        for(let i in _currentAnswerIds) {
            let a_id = _currentAnswerIds[i];
            console.log('a_id: ' + a_id);
            let updatedClass = a_id === answerId ? 'card border-success' : 'card border-danger';
            document.getElementById('a-div-' + a_id).setAttribute('class', updatedClass);
            document.getElementById('a-button-' + a_id).hidden = true;
        }
    }
    if(terminal) {
        let action = getAction(followUp);
        // console.log('terminal: ' + a.text);
        showSelectedAction(action);
    } else {
        let q = getQuestion(followUp);
        // console.log('terminal: ' + q.text);
        _currentQuestion = followUp;
        showCurrentQuestionAndAnswers();
    }
}

function restart() {
    console.log('Restart');
    _currentQuestion = _questionsJson.mainId;
    cleanUpQuestionsAndAnswers();
    showCurrentQuestionAndAnswers();
}

function showErrorMessage(errorMessage) {
    console.error(errorMessage);
}

function startSpinner() {
    document.getElementById('spinner').hidden = false;
}

function stopSpinner() {
    document.getElementById('spinner').hidden = true;
}

function setCookie(name, value, expiryInDays = 1) {
    let date = new Date();
    date.setTime(date.getTime() + (expiryInDays * 24 * 60 * 60 * 1000));
    let expires = "expires="+ date.toUTCString();
    document.cookie = name + "=" + value + ";" + expires + ";path=/";
}

function getCookie(name) {
    let amendedName = name + "=";
    let decodedCookie = decodeURIComponent(document.cookie);
    let cookies = decodedCookie.split(';');
    for(let i = 0; i <cookies.length; i++) {
        let c = cookies[i];
        while (c.charAt(0) === ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(amendedName) === 0) {
            return c.substring(amendedName.length, c.length);
        }
    }
    return "";
}

let questionTemplateHtml =
    '<div class="row">' +
    '    <div class="col border border-info" style="margin: 8px;">' +
    '        <p id="q" class="h5" style="margin-top: 8px;">Question</p>' +
    '        <p class="mb-0">%question%</p>' +
    '        <hr class="bg-secondary"/>' +
    '       %answers%' +
    '    </div>' +
    '</div>';

let answerTemplateHtml =
    '<div id="a-div-%answer-id-div%" class="card border-secondary" style="padding: 8px; margin-bottom: 8px;">' +
    '    <p>%answer%</p>' +
    '    <button id="a-button-%answer-id-button%" type="button" class="btn" onclick="%submit_answer%">Select</button>' +
    '</div>';

let actionTemplateHtml =
    '<div class="row">' +
    '   <div class="col border border-primary" style="margin: 8px;">' +
    '       <p id="q" class="h5" style="margin-top: 8px;">Action</p>' +
    '       <p class="mb-0">%action%</p>' +
    '   </div>' +
    '</div>';
